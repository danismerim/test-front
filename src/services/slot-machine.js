import config from './config';

export const getSpinResult = async () => {
    try {
        const res = await fetch(`${config.endpointBackend}slot-machine`, {
            headers: {
                accept: "application/json",
                "accept-language": "",
                "content-type": "application/json",
            },
            method: "GET"
        });

        const result = await res.json();

        if (!res || res.status > 299) {
            return { error: true, msg: result.message };
        }

        return { error: false, response: result };
    } catch (error) {
        return { error: true, msg: error };
    }
}