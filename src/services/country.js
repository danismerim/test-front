import config from './config';

export const getCountry = async (countryName) => {
    try {
        const res = await fetch(`${config.endpointBackend}countries/search?countryName=${countryName}`, {
            headers: {
                accept: "application/json",
                "accept-language": "",
                "content-type": "application/json",
            },
            method: "GET"
        });

        const result = await res.json();

        if (!res || res.status > 299) {
            return { error: true, msg: result.message };
        }

        return { error: false, response: result };
    } catch (error) {
        return { error: true, msg: error };
    }
}

export const searchCountriesList = async (countryNames) => {
    try {
        const res = await fetch(`${config.endpointBackend}countries/search-list?countryNames=${countryNames}`, {
            headers: {
                accept: "application/json",
                "content-type": "application/json",
            },
            method: "GET"
        });

        const result = await res.json();

        if (!res || res.status > 299) {
            return { error: true, msg: result.message };
        }

        return { error: false, response: result };
    } catch (error) {
        return { error: true, msg: error };
    }
}

export const getCountries = async () => {
    try {
        const res = await fetch(`${config.endpointBackend}countries/`, {
            headers: {
                accept: "application/json",
                "content-type": "application/json",
            },
            method: "GET"
        });

        const result = await res.json();

        if (!res || res.status > 299) {
            return { error: true, msg: result.message };
        }

        return { error: false, response: result };
    } catch (error) {
        return { error: true, msg: error };
    }
}