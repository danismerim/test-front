import config from './config';

export const login = async (credentials) => {
    try {
        const res = await fetch(`${config.endpointBackend}login`, {
            headers: {
                accept: "application/json",
                "accept-language": "",
                "content-type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(credentials),
        });

        const result = await res.json();

        if (!res || res.status > 299) {
            return { error: true, msg: result.message };
        }

        return { error: false, response: result };
    } catch (error) {
        return { error: true, msg: error };
    }
};