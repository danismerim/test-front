import React from "react";

function Country({ country }) {

    return (
        <>
            {country &&
                <div className="col-12 col-md-6 col-lg-4 mt-2">
                    <div className={"card h-100" + (country.error && " error" || '')}>
                        <div className="card-body row align-items-center">
                            <span className="col-md-8" style={{ fontSize: "2em" }}> {country.name}</span>
                            <img
                                className="col-md-4"
                                src={country.flag}
                                alt={"flag " + country.name}
                                style={{ maxHeight: "50px", objectFit: 'contain' }} />
                        </div>
                    </div>
                </div>
            }
        </>
    )
}

export default Country;