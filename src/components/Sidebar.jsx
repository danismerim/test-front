import React from "react";

export default () => {
    return (
        <nav className="sidebar col-md-2">
            <div className="sidebar-header">
                <div className="sidebar-logo">
                    <img src="logo.png" alt="logo kanon" />
                    <br />
                    <strong >Test</strong>
                </div>
                <span> <strong>Front End React Developer</strong></span>
                <br />
                <span>
                    <a href="https://www.linkedin.com/in/danielle-ismerim-743181101/" target="_blank">
                        <strong>Danielle Ismerim</strong>
                    </a>
                </span>
            </div>
            <div className="sidebar-body">
                <ul className="menu">
                    <li>
                        <a href="/question-1" className="nav-link">
                            Question 1
                        </a>
                    </li>
                    <li>
                        <a href="/question-2" className="nav-link">
                            Question 2
                        </a>
                    </li>
                    <li>
                        <a href="/question-3" className="nav-link">
                            Question 3
                        </a>
                    </li>
                    <li>
                        <a href="/question-4" className="nav-link">
                            Question 4
                        </a>
                    </li>
                    <li>
                        <a href="/question-5" className="nav-link">
                            Question 5
                        </a>
                    </li>
                    <li>
                        <a href="/question-6" className="nav-link">
                            Question 6
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    )
}