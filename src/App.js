import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Question1 from './routes/Question1';
import Question2 from './routes/Question2';
import Question3 from './routes/Question3';
import Question4 from './routes/Question4';
import Question5 from './routes/Question5';
import Question6 from './routes/Question6';
import Sidebar from "./components/Sidebar";

function App() {
  return (
    <div className="main-wrapper row">
      <Sidebar />
      <div className="page-wrapper col-md-10">
        <Router>
          <Switch>
            <Route path="/question-1">
              <Question1 />
            </Route>
            <Route path="/question-2">
              <Question2 />
            </Route>
            <Route path="/question-3">
              <Question3 />
            </Route>
            <Route path="/question-4">
              <Question4 />
            </Route>
            <Route path="/question-5">
              <Question5 />
            </Route>
            <Route path="/question-6">
              <Question6 />
            </Route>
            <Route path="/">
              <Question1 />
            </Route>
          </Switch>
        </Router>
      </div>
    </div>
  );
}

export default App;
