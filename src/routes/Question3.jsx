import React, { useState, useEffect } from "react";
import Country from "../components/Country";
import { getCountries } from '../services/country'

function Question2() {
    const [allCountries, setAllCountries] = useState([]);
    const [countriesFiltered, setCountriesFiltered] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        getAllCountries();
    }, []);

    const getAllCountries = async () => {
        const getCountryRes = await getCountries();
        if (!getCountryRes.error) {
            setAllCountries(getCountryRes.response);
            setCountriesFiltered(getCountryRes.response);
            setIsLoading(false);
        } else {
            setAllCountries({ name: getCountryRes.msg, flag: "not-found.png", error: true });
            setIsLoading(false);
        }
    }

    const filterCountry = (country) => {
        const countries = allCountries.filter(c => c.name.toLowerCase().includes(country.toLowerCase()));
        setCountriesFiltered([...countries]);

        if (!countries || (countries && countries.length === 0)) {
            setCountriesFiltered([{ name: "Country not found :(", flag: "not-found.png", error: true }])
        }
    }

    return (
        <>
            <div className="question">
                <form onSubmit={(e) => {
                    e.preventDefault();
                    filterCountry();
                    return false;
                }}>
                    <div>
                        <strong>Question 3) </strong>
                        <p>List all the countries on the client and create a field to filter them by name</p>
                        <ul>
                            <li>On the backend, use this API for countries https://restcountries.eu/</li>
                        </ul>
                    </div>

                    <div className="row">
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <input
                                className="form-control"
                                onKeyUp={(e) => {
                                    filterCountry(e.target.value);
                                    if (e.target.value == '') {
                                        setCountriesFiltered(allCountries);
                                    }
                                }}
                            />
                        </div>

                        {isLoading && <div className="spinner-border" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                        }

                    </div>
                </form>

                {countriesFiltered && countriesFiltered.length > 0 &&
                    <>
                        <div className="row mt-4">
                            {countriesFiltered.map((country, i) => (
                                <Country key={i} country={country} />
                            ))}
                        </div>
                    </>
                }
            </div>
        </>
    );
}

export default Question2;
