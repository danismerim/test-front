import React, { useState } from "react";
import { postUser } from '../services/user'

function Question4() {
    const [user, setUser] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [isInvalid, setIsInvalid] = useState(false);
    const [isSaved, setIsSaved] = useState(false);
    const [errorMessage, setErrorMessage] = useState();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUser((prevState) => ({
            ...prevState,
            [name]: value,
        }));
        setIsSaved(false);
        setIsInvalid(false);
    };


    const saveUser = async () => {
        setErrorMessage(null);
        if (!user.name || !user.email || !user.password) {
            setIsInvalid(true);
            setIsLoading(false);
            setIsSaved(false);

            return;
        }
        const getCountryRes = await postUser(user);
        if (!getCountryRes.error) {
            setIsLoading(false);
            setIsSaved(true);
            setIsInvalid(false);

        } else {
            setErrorMessage(getCountryRes.msg);
            setIsInvalid(true);
            setIsLoading(false);
            setIsSaved(false);
        }
    }

    return (
        <>
            <div className="question">
                <form onSubmit={async (e) => {
                    e.preventDefault();
                    setIsLoading(true)
                    await saveUser()
                    return false;
                }}>
                    <div>
                        <strong>Question 4) </strong>
                        <p>Create a register form with fields name, email and password. Must have full validation and fields are
                        required.
                        <br />
                        Send to the back end and save the user info in memory or any database (SQL or NoSQL).</p>
                    </div>

                    {isInvalid &&
                        <div className="col-lg-4 col-md-6 col-sm-6 form-result error">
                            {errorMessage ? errorMessage : 'Please fill in all fields.'}
                        </div>
                    }
                    {isSaved &&
                        <div className="col-lg-4 col-md-6 col-sm-6 form-result success">
                            User Created
                            {/* (check the logs on the api) */}
                        </div>
                    }
                    <div className="row">
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <label>Name</label>
                            <input
                                className="form-control"
                                value={user.name || ""}
                                onChange={handleChange}
                                name="name"
                            />
                        </div>
                    </div>
                    <div className="row mt-1">
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <label>Email</label>
                            <input
                                autoComplete="off"
                                className="form-control"
                                value={user.email || ""}
                                onChange={handleChange}
                                name="email"
                            />
                        </div>
                    </div>
                    <div className="row mt-1">
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <label>Password</label>
                            <input
                                type="password"
                                className="form-control"
                                value={user.password || ""}
                                onChange={handleChange}
                                name="password"
                            />
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col-lg-4 col-md-4 col-sm-6">
                            <button
                                className="btn btn-dark"
                                type="submit"
                                style={{ width: '100%' }}>
                                {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"> </span>}  Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}

export default Question4;
