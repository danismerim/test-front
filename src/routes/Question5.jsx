import React, { useState } from "react";
import { login } from '../services/auth'

function Question5() {
    const [user, setUser] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [isInvalid, setIsInvalid] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [errorMessage, setErrorMessage] = useState(false);
    const [token, setToken] = useState(false);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUser((prevState) => ({
            ...prevState,
            [name]: value,
        }));
        setIsSuccess(false);
        setIsInvalid(false);
    };


    const saveUser = async () => {
        setErrorMessage(null);
        if (!user.email || !user.password) {
            setIsInvalid(true);
            setIsLoading(false);
            setIsSuccess(false);

            return;
        }
        const getCountryRes = await login(user);
        if (!getCountryRes.error && getCountryRes.response) {
            setIsLoading(false);
            setIsSuccess(true);
            setIsInvalid(false);
            setToken(getCountryRes.response && getCountryRes.response.token)
        } else {
            setErrorMessage(getCountryRes.msg);
            setIsInvalid(true);
            setIsLoading(false);
            setIsSuccess(false);
        }
    }

    return (
        <>
            <div className="question">
                <form onSubmit={async (e) => {
                    e.preventDefault();
                    setIsLoading(true)
                    await saveUser()
                    return false;
                }}>
                    <div>
                        <strong>Question 5) </strong>
                        <p>Using the account info from Question 4, create a login form which returns a JWT token from the
                        backend and uses that to maintain a session.</p>
                    </div>

                    {isInvalid &&
                        <div className="col-lg-4 col-md-6 col-sm-6 form-result error">
                            {errorMessage ? errorMessage : 'Please fill in all fields.'}
                        </div>
                    }
                    {isSuccess &&
                        <div className="col-lg-4 col-md-6 col-sm-6 form-result success">
                            <span>Token: {token}</span>
                        </div>
                    }
                    <div className="row mt-1">
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <label>Email</label>
                            <input
                                autoComplete="off"
                                className="form-control"
                                value={user.email || ""}
                                onChange={handleChange}
                                name="email"
                            />
                        </div>
                    </div>
                    <div className="row mt-1">
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <label>Password</label>
                            <input
                                type="password"
                                className="form-control"
                                value={user.password || ""}
                                onChange={handleChange}
                                name="password"
                            />
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col-lg-4 col-md-4 col-sm-6">
                            <button
                                className="btn btn-dark"
                                type="submit"
                                style={{ width: '100%' }}>
                                {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"> </span>}  Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}

export default Question5;
