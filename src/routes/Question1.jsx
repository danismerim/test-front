import React, { useState } from "react";
import Country from "../components/Country";
import { getCountry } from '../services/country'

function Question1() {
    const [countryName, setCountryName] = useState();
    const [country, setCountry] = useState();
    const [isLoading, setIsLoading] = useState(false);

    const searchCountry = async () => {
        const getCountryRes = await getCountry(countryName);
        if (!getCountryRes.error) {
            setCountry(getCountryRes.response);
            setIsLoading(false);
        } else {
            setCountry({ name: "Country not found :(", flag: "not-found.png", error: true });
            setIsLoading(false);
        }
    }

    return (
        <>
            <div className="question">
                <form onSubmit={async (e) => {
                    e.preventDefault();
                    setIsLoading(true)
                    await searchCountry()
                    return false;
                }}>
                    <div>
                        <strong>Question 1) </strong>
                        <p>Send a request from the frontend to the backend with a string ‘countryName’ which the backend will
                        use to return a single matched country.</p>
                        <ul>
                            <li>On the backend, use this API for countries https://restcountries.eu/</li>
                            <li>Display the country on the frontend</li>
                        </ul>
                    </div>

                    <div className="form-inline">
                        <div>
                            <input
                                defaultValue={countryName}
                                className="form-control"
                                onChange={(e) => setCountryName(e.target.value)} />

                        </div>
                        <button
                            className="btn btn-dark ml-1"
                            type="submit">
                            {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"> </span>}  Search
                            </button>
                    </div>
                </form>
                {country &&
                    <>
                        <div className="row mt-4">
                            <Country country={country} />
                        </div>
                    </>
                }
            </div>
        </>
    );
}

export default Question1;
