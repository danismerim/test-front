import React, { useState } from "react";
import Country from "../components/Country";
import { searchCountriesList } from '../services/country'

function Question1() {
    const [countryName, setCountryName] = useState("");
    const [countryNames, setCountryNames] = useState([]);
    const [countries, setCountries] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const searchCountries = async () => {
        let contriesToSearch = countryNames.join(';');
        if (!countryNames || (countryNames && countryNames.length === 0)) {
            contriesToSearch = countryName;
        }
        const searchCountryRes = await searchCountriesList(contriesToSearch);
        if (!searchCountryRes.error) {
            setCountries(searchCountryRes.response);
            setIsLoading(false);
        } else {
            setCountries([{ name: searchCountryRes.msg, flag: "not-found.png", error: true }]);
            setIsLoading(false);
        }
    }

    const addCountry = (country) => {
        setCountryNames([...countryNames, country]);
        setCountryName('');
    }

    const removeCountry = (index) => {
        countryNames.splice(index, 1);
        setCountryNames([...countryNames]);
    }

    return (
        <>
            <div className="question">
                <div>
                    <strong>Question 2) </strong>
                    <p> Send a request from the frontend to the backend with an array of strings ‘countryNames’ which will
                    return a list of countries partly matching any of the strings.</p>
                    <ul>
                        <li>On the backend, use this API for countries https://restcountries.eu/</li>
                        <li>Display the countries on the frontend</li>
                    </ul>
                </div>

                <div className="row form-inline">
                    <div className="">
                        <input
                            value={countryName}
                            className="form-control"
                            onChange={(e) => setCountryName(e.target.value)} />
                    </div>
                    <button
                        className="btn btn-secondary ml-1"
                        onClick={() => {
                            addCountry(countryName);
                        }}>
                        Add
                        </button>
                    <button
                        className="btn btn-dark ml-1"
                        type="submit"
                        onClick={async (e) => {
                            e.preventDefault();
                            setIsLoading(true)
                            await searchCountries()
                        }}>
                        {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"> </span>}  Search
                    </button>
                </div>

                {countryNames && countryNames.length > 0 &&
                    <div className="row mt-3">
                        {countryNames.map((country, i) => (
                            <div key={i} className="col-lg-2 col-md-4 col-sm-6 country-search mr-2 mt-1">
                                {country}
                                <i
                                    className="fa fa-trash-o"
                                    onClick={() => {
                                        removeCountry(i)
                                    }}
                                ></i>
                            </div>
                        ))}
                    </div>
                }

                {countries && countries.length > 0 &&
                    <>
                        <div className="row mt-4">
                            {countries.map((country, i) => (
                                <Country key={i} country={country} />
                            ))}
                        </div>
                    </>
                }
            </div>
        </>
    );
}

export default Question1;
