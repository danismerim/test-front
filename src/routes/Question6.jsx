import React, { useState } from "react";
import { getSpinResult } from '../services/slot-machine'

function Question6() {
    const [slotMachineResult, setSlotMachineResult] = useState(["", "", ""]);
    const [isLoading, setIsLoading] = useState(false);
    const [coins, setCoins] = useState(20);
    const [isOver, setIsOver] = useState(false);

    const spinMachine = async () => {
        const getSpinRes = await getSpinResult();
        setSlotMachineResult(getSpinRes.response.result);
        const coinsUpdated = coins + getSpinRes.response.coins - 1;
        setCoins(coinsUpdated);
        if (coinsUpdated <= 0) {
            setIsOver(true);
        }
        setSlotMachineResult(getSpinRes.response.result);
        setIsLoading(false);
    }

    return (
        <>
            <div className="question">
                <div>
                    <strong>Question 6) </strong>
                    <p>Considering a Slot machine defined like this:</p>
                    <ul>
                        <li>Reel1: [“cherry”, ”lemon”, “apple”, ”lemon”, “banana”, “banana”, ”lemon”, ”lemon”]</li>
                        <li>Reel2: [”lemon”, “apple”, ”lemon”, “lemon”, “cherry”, “apple”, ”banana”, ”lemon”]</li>
                        <li>Reel3: [”lemon”, “apple”, ”lemon”, “apple”, “cherry”, “lemon”, ”banana”, ”lemon”]</li>
                    </ul>
                    <p>The user starts with 20 coins. Each spin will cost the user 1 coin.</p>
                    <strong>
                        Please note that slot machines only consider pairs a match if they are in order from left to right. <br />
                            Eg:<br />
                            Apple, Cherry, Apple - no win<br />
                            Apple, Apple, Cherry - win<br />
                            Rewards<br />
                    </strong>
                    <ul>
                        <li>3 cherries in a row: 50 coins, 2 cherries in a row: 40 coins</li>
                        <li>3 Apples in a row: 20 coins, 2 Apples in a row: 10 coins</li>
                        <li>3 Bananas in a row: 15 coins, 2 Bananas in a row: 5 coins</li>
                        <li>3 lemons in a row: 3 coins</li>
                    </ul>
                    <p>
                        Create an endpoint on the backend that when called by the frontend will return the result of the spin
                        and the coins the player won.
                        </p>
                </div>

                <div className="spin-machine">
                    {slotMachineResult &&
                        <div className="row justify-content-center">
                            {slotMachineResult.map((res, i) => (
                                <div key={i} className="item">
                                    {res}
                                </div>
                            ))}
                        </div>
                    }
                    <div className="row align-items-center justify-content-center">
                        <div className={"coins" + (isOver && " error" || '')}>
                            Coins
                            <br />
                            {coins}
                        </div>
                        <div className="ml-5 mt-3" >
                            <button
                                className={"btn" + (isOver && " error" || '')}
                                onClick={() => {
                                    setIsLoading(true);
                                    spinMachine();
                                }}
                                disabled={isOver}
                            >
                                {isLoading && <span className="spinner-border spinner-border-sm mt-3" role="status" aria-hidden="true"> </span>}
                                {isOver ? "Game Over :(" : " SPIN"}
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </>
    );
}

export default Question6;
